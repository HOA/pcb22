# PCB22

## Aufbau

1. Löte die 3 Widerstände `R1`, `R2`, `R3` (331 330Ω). Die Ausrichtung ist egal.
2. Löte den Schalter auf `SW1`. Die Ausrichtung ist egal. Löte zuerst nur ein Pad an, bis der Schalter an der richtigen Stelle sitzt.
3. Trenne die Verbindung zwischen den Pads von `JP1`. Dafür kannst du ein Skalpell benutzen.
4. Löte den Battery halter `BT1` auf.
5. Kürze die LED-Beinchen auf eine geeignete Länge.
6. Löte die LEDs auf und achte auf den + und - Pol.
7. Stecke die Batterie ein.
8. Leuchtet es? Gut. Raucht es? Doof.
9. Entferne die Folie vom Plexy-Glas-Specht. Auf beiden Seiten.
10. Klebe den Specht auf die Rückseite vom PCB

## Quelldateien

- [Schematic](pcb/releases/R2/schematic.pdf)
